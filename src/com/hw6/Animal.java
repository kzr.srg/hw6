package com.hw6;

 abstract public class Animal {
    private static int animalCount;
    private String typeAnimal;
    private int runLimit;
    private int swimLimit;

    public Animal(int runLimit, int swimLimit, String typeAnimal) {
        animalCount++;
        this.swimLimit = swimLimit;
        this.runLimit = runLimit;
        this.typeAnimal = typeAnimal;
    }
    public static  void showSumAnimals(){
        System.out.println("Всего животных:"+animalCount);
    }
    public  void run(int distance){
        if (runLimit == 0 ) {
            System.out.println("Животное "+typeAnimal+" не умеет бегать! :0");
        }else if (distance>runLimit) {
            System.out.println("Животное "+typeAnimal+" пробежало только "+runLimit+"м. и больше не может... :(");
        }else {
            System.out.println("Животное "+typeAnimal+" пробежало "+distance+ "м. Ура! :)");
        }
    }
    public  void swim(int distance){
        if (swimLimit == 0 ) {
            System.out.println("Животное "+typeAnimal+" не умеет плавать! :0");
        }else if (distance>swimLimit) {
            System.out.println("Животное "+typeAnimal+" проплыло только "+swimLimit+ "м. и больше не может... :(");
        }else {
            System.out.println("Животное "+typeAnimal+" проплыло "+distance+ "м. Ура! :)");
        }
    }
}
