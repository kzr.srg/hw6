package com.hw6;

public class Main {
    public static void main(String[] args) {

        Animal dog1 = new Dog();
        Animal dog2 = new Dog();

        Animal cat1 = new Cat();
        cat1.swim(10);

        dog1.swim(50);
        dog1.swim(5);
        dog2.swim(10);

        Animal.showSumAnimals();
        Cat.showSumCats();
        Dog.showSumDogs();
    }
}
/*      Животное кот не умеет плавать! :0
        Животное собака проплыло только 10м. и больше не может... :(
        Животное собака проплыло 5м. Ура! :)
        Животное собака проплыло 10м. Ура! :)
        Всего животных:3
        Всего котов:1
        Всего собак:2
 */
